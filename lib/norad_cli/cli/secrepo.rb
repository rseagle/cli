# frozen_string_literal: true

require 'thor'
require 'git'
require 'docker'

class Repo < Thor
  include Thor::Actions

  def self.source_root
    File.join(File.dirname(File.expand_path(__FILE__)), '../templates/')
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  desc 'create REPONAME', 'Create a new norad security test repository called REPONAME'
  def create(repo_name)
    say 'Initializing a new norad security test repository'

    # Initialize a new git repository
    Git.init(repo_name)

    # Create the necessary directories
    %w[base spec sectests].each do |dirrepo_name|
      empty_directory "#{repo_name}/#{dirrepo_name}"
    end

    # Ask about licensing
    if yes?('License the repo under Apache 2?')
      options[:year] = Date.today.year
      options[:company] = ask('Who is the copyright holder (Default: Cisco Systems, Inc.)?')
      options[:company] = 'Cisco Systems, Inc.' if options[:company].empty?
      template 'LICENSE.erb', "#{repo_name}/LICENSE"
    end

    # Copy the necessary root files
    copy_file 'ci/.gitlab.ci.yml', "#{repo_name}/.gitlab.ci.yml"
    copy_file '.gitignore', "#{repo_name}/.gitignore"
    copy_file 'CONTRIBUTING.md', "#{repo_name}/CONTRIBUTING.md"
    copy_file 'README.md', "#{repo_name}/README.md"

    # Copy the spec helper
    copy_file 'spec/spec_helper.rb', "#{repo_name}/spec/spec_helper.rb"
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength
end
