# frozen_string_literal: true

require 'thor'
require 'norad_cli/cli/secrepo'
require 'norad_cli/cli/sectest'

module NoradCli
  class CLI < Thor
    # Register all of the subcommands
    register(Repo, 'repo', 'repo <command>', 'Commands for norad security repos housing security tests.')
    register(Sectest, 'sectest', 'sectest <command>', 'Commands to create new security tests.')
  end
end
