# frozen_string_literal: true

module NoradCli
  class ResultsServer
    attr_accessor :container

    def initialize(test_results_server_image)
      @container = Docker::Container.create(
        Image: test_results_server_image,
        HostConfig: { PublishAllPorts: true }
      )
    end

    def start
      @container.start
      sleep 5 # sleep rather than wait since we are daemonizing a containe
      refresh
    end

    def refresh
      @container.refresh! # get more details
    end

    def shutdown
      @container.stop
      @container.delete(force: true)
    end

    def host_port
      @container.info['NetworkSettings']['Ports']['3000/tcp'].first['HostPort']
    end
  end
end
