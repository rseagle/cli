# Norad

The norad gem is a command line interface (CLI) to create new security tests and interface with an instance of norad.  Currently, the project only contains code to create new security tests.

## Dependencies

To use this tool to create new security tests, the only dependencies is Docker.  The easiest way to install Docker is through the community edition.

[Docker Community Edition](https://www.docker.com/community-edition)

The above site contains all the necessary information to install Docker on a Mac, Windows, or Linux machines.

## Installation

This utility can be installed simply by:

```
$ gem install norad_cli
```

Once installed, a norad executable should be available.

## Usage

The norad executable has several subcommands to aid a developer in creating security tests.  The tool contains a help interface to provide information about the available subcommand and options.  An example of the available help:

```
$ norad help
Commands:
  norad help [COMMAND]     # Describe available commands or one specific command
  norad repo <command>     # Commands for norad security repos housing security tests.
  norad sectest <command>  # Commands to create new security tests.
```

Additional help for subcommands (e.g. repo or sectest) is available by:

```
$ norad repo help
Commands:
  norad repo create REPONAME  # Create a new norad security test repository called REPONAME
  norad repo help [COMMAND]   # Describe subcommands or one specific subcommand

```

Individual help for a command is available with:

```
$ norad repo create --help
Usage:
  norad create REPONAME

Create a new norad security test repository called REPONAME
```

### Repo Subcommand

The repo subcommand creates a new git repository to house new security tests.  It is a helper command which ensures new repositories conform to standard layout and ci best practices.  To create a new repository for housing multiple security tests:

```
$ norad repo create asig-security
Initializing a new norad security test repository
      create  asig-security/base
      create  asig-security/spec
License the repo under Apache 2? y
Who is the copyright holder (Default: Cisco Systems, Inc.)?
      create  asig-security/LICENSE
      create  asig-security/.gitlab.ci.yml
      create  asig-security/.gitignore
      create  asig-security/CONTRIBUTING.md
      create  asig-security/README.md
```

The user will be prompted whether to license the code under Apache 2 and for the copyright holder.  The norad framework is released under the Apache 2 license, test content is not required to be licensed the same.

As shown, a new directory, asig-security, has been created and multiple files created.  Changing directory into asig-security and running git status shows a new git repository waiting for an initial commit:

```
$ git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

    .gitignore
    .gitlab.ci.yml
    CONTRIBUTING.md
    LICENSE
    README.md

nothing added to commit but untracked files present (use "git add" to track)
```

The repo command is only useful when starting an entirely new repository to house multiple security tests!  For general security test development inside of an existing repository see the below sectest subcommand.

### Sectest Subcommand

The sectest subcommand is where general security test tool development occurs.  The sectest subcommand can scaffold a new security test, build security test docker images, test images, execute a security test, and create a seed file for important into a Norad instance.  A listing of the current commands is:

```
$ norad help sectest
Commands:
  norad sectest build                       # Build all sectest images and specs for the entire repository
  norad sectest build:all SECTESTNAME       # Build sectest images for SECTESTNAME and all testing images for SECTESTNAME
  norad sectest build:image SECTESTNAME     # Build the docker image for the security test SECTESTNAME
  norad sectest build:specs SECTESTNAME     # Build the spec images (test images) for the security test SECTESTNAME
  norad sectest execute SECTESTNAME         # Execute SECTESTNAME against an arbitrary target
  norad sectest help [COMMAND]              # Describe subcommands or one specific subcommand
  norad sectest scaffold TESTNAME           # Create a new security test with standard files + testing
  norad sectest seed                        # Create the containers.rb seed to import into the api
  norad sectest spec                        # Run all rspec tests for the entire repo (all sectests)
  norad sectest spec:image SECTESTNAME      # Run the rspec tests for SECTESTNAME
  norad sectest validate                    # Validate all manifest.yml and readme.md
  norad sectest validate:image SECTESTNAME  # Validate SECTESTNAME manifest.yml and readme.md
```

### Sectest Execute Command

The execute command enables users to run any SECTESTNAME container against an arbitrary target.  To see the available options for the subcommand, run:

```
$ norad sectest -h execute
Usage:
  norad execute SECTESTNAME

Options:
  -d, [--debug], [--no-debug]    # Turn on debugging messages (e.g. Docker build logs to stdout)
  -f, [--format], [--no-format]  # Print the JSON results formatted
  -r, [--registry=REGISTRY]      # The Docker registry for Docker images
                                 # Default: norad-registry.cisco.com:5000
  -v, [--version=VERSION]        # The version of the sectest container to build
                                 # Default: latest

Execute SECTESTNAME against an arbitrary target
```

Since the execute command runs arbitrary SECTESTNAME containers, it must support dynamically setting options for those containers.  Therefore, the command supports gathering and printing help with any SECTESTNAME container.  To get the help for a container run:

```
$ norad sectest execute SECTESTNAME -h
```

The execute subcommand reads the SECTESTNAME's manifest.yml to determine all available options.  An example of this behavior is:

```
$ norad sectest execute sec-ops-sudo -h
Usage:
  norad execute sec-ops-sudo

Options:
  -d, [--debug], [--no-debug]    # Turn on debugging messages (e.g. Docker build logs to stdout)
  -f, [--format], [--no-format]  # Print the JSON results formatted
  -r, [--registry=REGISTRY]      # The Docker registry for Docker images
                                 # Default: norad-registry.cisco.com:5000
  -v, [--version=VERSION]        # The version of the sectest container to build
                                 # Default: latest
  -t, [--target=TARGET]          # The IP or FQDN of the host to test
  -u, [--ssh-user=SSH_USER]      # If the sectest requires authentication, then the username for authentication
  -p, [--port=PORT]              # The port to use for testing
  -k, [--ssh-key=SSH_KEY]        # If the sectest requires authentication, then the path to the ssh key file

Execute sec-ops-sudo against an arbitrary target
```

## Development

1. Use `bundler exec rake spec` to ensure you are passing all tests.
1. Build gem with `bundle exec rake build`.
2. Install gem locally with `bundle exec rake install`.
  - This will override existing norad_cli gem
  - Remove gem with `gem uninstall norad_cli`

If you are unfamiliar with developing Ruby Gems but would like to contribute,
please visit:
  - http://guides.rubygems.org
  - http://bundler.io/v1.12/guides/creating_gem.html

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/norad/cli
