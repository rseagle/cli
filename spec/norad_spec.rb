# frozen_string_literal: true

require 'spec_helper'

describe NoradCli do
  it 'has a version number' do
    expect(NoradCli::VERSION).not_to be nil
  end
end
