# frozen_string_literal: true

require 'spec_helper'

describe NoradCli::UiSeedGenerator do
  context 'when processing is successful' do
    let(:seed_path) { "#{Dir.pwd}/seed.yml" }
    after(:each) { File.delete(seed_path) if File.exist?(seed_path) }

    it 'can process all manifest_paths' do
      described_class.new([]).process!

      expect(File.exist?(seed_path)).to be true

      configurations = YAML.safe_load(File.read(seed_path))

      expect(configurations.map { |config| config['name'] }.to_set).to eq(
        ['vuls:0.0.1', 'zap-xss:0.0.1', 'sec-ops-strength:0.0.1'].to_set
      )
    end

    it 'accepts arbitrary manifest_paths as parameters' do
      manifest_paths = [
        File.expand_path('../../support_files/docker_images/sectests/vuls/manifest.yml', __FILE__)
      ]
      described_class.new(manifest_paths).process!

      expect(File.exist?(seed_path)).to be true

      configurations = YAML.safe_load(File.read(seed_path))

      expect(configurations.count).to eq(1)
      expect(configurations[0]['name']).to eq('vuls:0.0.1')
    end

    it 'does not include attributes that are not needed' do
      path = File.expand_path('../../support_files/docker_images/sectests/sec-ops-strength/manifest.yml', __FILE__)

      described_class.new([path]).process!
      configurations = YAML.safe_load(File.read(seed_path))

      expect(configurations[0]).to_not be_key('modifies_target_data')
      expect(configurations[0]).to be_key('prog_args')
    end
  end

  context 'when processing fails' do
    it 'shows helpful error if manifest provided is not valid yaml' do
      path = File.expand_path('../../../Gemfile', __FILE__)
      expect(STDOUT).to receive(:puts).with(
        "Invalid manifest: #{path}. See Norad security test repositories page for guidelines."
      )

      described_class.new([path]).process!
    end

    it "shows helpful error if manifest provided doesn't exist" do
      path = File.expand_path('../../path/doesnt/exist/manifest.yml', __FILE__)
      expect(STDOUT).to receive(:puts).with("Could not find manifest provided: #{path}")

      described_class.new([path, path, path, path]).process!
    end

    it 'validates for required fields' do
      begin
        path = "#{Dir.pwd}/manifest.yml"

        File.write(path, <<-YAML)
          registry: norad-registry.cisco.com:5000
          version: 0.0.1
          default_config:
            cisco_enable_pw: ''
          test_types:
            - authenticated
          category: whitebox
          configurable: true
          modifies_target_data: true
          modifies_target_config: true
          modifies_service_data: true
          modifies_service_config: true
        YAML

        expect(STDOUT).to receive(:puts).with(
          "Invalid manifest: #{path}. Missing attributes: name, prog_args"
        )

        described_class.new([path]).process!
      ensure
        File.delete(path)
      end
    end
  end
end
